package com.vyuka.klient.websocket;

// import potřebných balíčků
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class websocket_client {

    private Socket clientSocket; // socket pro klients
    private Scanner in; // scanner pro načítání dat od uživatele

    //konstruktor třídy Client
    public websocket_client()
    {
        pripoj_se();
    }

        public void pripoj_se()
        {
            try
            {
                this.clientSocket = new Socket("localhost", 8081); // inicializace client socketu
                System.out.println("Spuštění klienta proběhlo úspěšně."); // zpráva o úspěšném spuštění
                this.in = new Scanner(System.in);
            }
            catch (UnknownHostException e) {
                    // e.printStackTrace();
                } catch (IOException e) {
                    // e.printStackTrace();
                }

        }

    public void odesli_zpravu(String zprava)
    {
        try {
                System.out.println("ZPRAVA?"+zprava);
                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(this.clientSocket.getOutputStream())); //vytvoření BufferedWriteru
                out.write(zprava + "\r\n"); // zapsání zprávy do BufferedWriteru
                out.flush(); // odeslání zprávy
                System.out.println("Zpráva: \"" + zprava + "\" odeslána."); // oznámení o úspěšném odeslání
            }
        catch (IOException ex)
            {
            //throw new RuntimeException(ex);
               this.pripoj_se();
                System.out.println(ex.getMessage());
            }
    }


    }
