package com.vyuka.klient;

import com.sun.management.OperatingSystemMXBean;
import com.vyuka.klient.websocket.websocket_client;
import org.json.JSONObject;
import java.lang.management.ManagementFactory;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimerTask;

public class uloha_casovace extends TimerTask {
    public websocket_client Client;
    public JSONObject VystupJson;
    private  OperatingSystemMXBean osBean ;
    static LocalDateTime DatumACas;
    static Double CPU_vyuziti;
    static String Mem_vyuziti;

    uloha_casovace(){
        this.osBean = ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class);
        VystupJson = new JSONObject();
        this.Client = new websocket_client();
    }

    @Override
    public void run() {
        DatumACas = LocalDateTime.now();
        CPU_vyuziti=osBean.getProcessCpuLoad() * 100;
        Mem_vyuziti= Double.toString(osBean.getFreePhysicalMemorySize());


        DecimalFormat df = new DecimalFormat("#.##");

        //=============================================================
        VystupJson = new JSONObject();
        VystupJson.put("Datum",DatumACas);
        VystupJson.put("CPU_vyuziti",df.format(CPU_vyuziti));
        VystupJson.put("MEM_vyuziti",Mem_vyuziti);

        //=============================================================
        System.out.println(VystupJson);
        // NASTAVENI PRO PRIPOJENI K SERVERU
        this.Client.odesli_zpravu(String.valueOf(VystupJson));
        //=============================================================



    }
}
