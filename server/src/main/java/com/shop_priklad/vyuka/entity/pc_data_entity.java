package com.shop_priklad.vyuka.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;

@Entity
@Table(name="tbl_pc_data")
public class pc_data_entity {
    private long id;
    private LocalDateTime datum;
    private String CPU_vyuziti;
    private String Mem_vyuziti;

        public pc_data_entity() {
        }

        public pc_data_entity(String datum, String CPU_vyuziti, String Mem_vyuziti) {

            this.datum       = LocalDateTime.parse(datum);
            this.CPU_vyuziti = CPU_vyuziti;
            this.Mem_vyuziti = Mem_vyuziti;
        }

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        public long getId() {return id;}
        public void setId(long id) {
            this.id = id;
        }

        @Column(name = "datum", nullable = false)
        public LocalDateTime getDatum() {return datum;}
        public void setDatum(LocalDateTime datum) {this.datum = datum;}

        @Column(name = "CPU_vyuziti", nullable = false)
        public String getCPU_vyuziti() {return CPU_vyuziti;}
        public void setCPU_vyuziti(String CPU_vyuziti) {this.CPU_vyuziti = CPU_vyuziti;}

        @Column(name = "Mem_vyuziti", nullable = false)
        public String getMem_vyuziti() {return Mem_vyuziti;}
        public void setMem_vyuziti(String mem_vyuziti) {Mem_vyuziti = mem_vyuziti;}

        @Override
        public String toString() {
            return "pc_data_entity{" +
                    "id=" + id +
                    ", datum='" + datum + '\'' +
                    ", CPU_vyuziti='" + CPU_vyuziti + '\'' +
                    ", MEM_vyuziti='" + Mem_vyuziti + '\'' +
                    '}';
        }

    }

