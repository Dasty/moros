package com.shop_priklad.vyuka.database;

import com.shop_priklad.vyuka.entity.pc_data_entity;

import java.sql.*;

public class DBConnect {
    String adresa;
    String port;
    String database;
    String username ="postgres";
    String password="parkoviste";
    private  Connection connection;


    public DBConnect(String adresa, String port, String database, String username, String password) {

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        this.adresa   = adresa;
        this.port     = port;
        this.database = database;
        this.username = username;
        this.password = password;
    }

    public Connection  getConnection(){
        try {
            System.out.println("PRIPOJUJI K POSTGRESSU");
            connection = DriverManager.getConnection("jdbc:postgresql://" + adresa +":"+port+ "/" + database, username, password);
            System.out.println(connection.getClientInfo());

        } catch (SQLException e) {
            System.err.println("Connection Failed! Check output console");
            e.printStackTrace();
        }

        return connection;
    }

    public long insertData(pc_data_entity data) {
        String SQL = "INSERT INTO pc_data.tbl_data(datum,cpu_vyuziti,mem_vyuziti) "
                + "VALUES(?,?,?)";

        long id = 0;

        try (Connection conn = this.getConnection();
             PreparedStatement pstmt = conn.prepareStatement(SQL,
                     Statement.RETURN_GENERATED_KEYS)) {

            pstmt.setTimestamp(1, Timestamp.valueOf(data.getDatum()));
            pstmt.setString(2, data.getCPU_vyuziti());
            pstmt.setString(3, data.getMem_vyuziti());

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return id;
    }

}
