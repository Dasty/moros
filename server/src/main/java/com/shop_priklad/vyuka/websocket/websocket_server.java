package com.shop_priklad.vyuka.websocket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;

import com.shop_priklad.vyuka.database.DBConnect;
import com.shop_priklad.vyuka.entity.pc_data_entity;
import org.json.JSONObject;
import org.springframework.boot.SpringApplication;

public class websocket_server {
        // privátní atribut serverSocket
        private ServerSocket serverSocket;
        // konstruktor třídy Server
        public websocket_server() {
            try {
                this.serverSocket = new ServerSocket(8081);
                System.out.println("Spuštění serveru proběhlo úspěšně.");
                Socket clientSocket = this.serverSocket.accept(); // vytvoření socketu

                BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()) );

                // vypisování zpráv od klienta
                while (true) {
                    JSONObject zaznam_dat= new JSONObject(in.readLine()); // načtení zprávy od klienta do proměnné

                    System.out.println(zaznam_dat); // vypsání zprávy klienta z proměnné

                    pc_data_entity data = new pc_data_entity
                                                (
                                                        (String) zaznam_dat.get("Datum"),
                                                        (String) zaznam_dat.get("CPU_vyuziti"),
                                                        (String) zaznam_dat.get("MEM_vyuziti")
                                                );


                    String adresa ="localhost";
                    String database = "postgres";
                    String port ="5432";
                    String user="postgres";
                    String password ="letadlo";

                    System.out.println("CHYSTÁM SE PRIPOJIT:");
                    DBConnect  dbConnect = new DBConnect(adresa,port,database,user,password);

                    long id =dbConnect.insertData(data);
                    System.out.println("ULOZENY ZAZNAM MA ID:"+id);

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }





