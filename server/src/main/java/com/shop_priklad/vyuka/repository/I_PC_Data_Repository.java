package com.shop_priklad.vyuka.repository;

import com.shop_priklad.vyuka.entity.pc_data_entity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface I_PC_Data_Repository extends JpaRepository<pc_data_entity, Long>  {
}
